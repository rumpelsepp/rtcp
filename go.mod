module git.sr.ht/~rumpelsepp/rtcp

go 1.14

require (
	git.sr.ht/~rumpelsepp/helpers v0.0.0-20200603092348-de954e3a9250
	git.sr.ht/~rumpelsepp/socks5 v0.0.0-20200624144959-822cedc392bc
	git.sr.ht/~sircmpwn/getopt v0.0.0-20191230200459-23622cc906b3
	github.com/Fraunhofer-AISEC/penlog v0.1.2-0.20200624142937-c5bc9405e8ab
)
